<?php
/**
 * @file
 * Contains base functionality to hooking into the checkout panes
 * forms and adding information on the checkout panes themself.
 */

define('COMMERCE_CHECKOUT_INFORMATION_VARIABLE_PREFIX', 'commerce_checkout_info_');

/**
 * Implements hook_form_alter().
 *
 * Hook into all checkout pane forms and allow the user to type
 * a information box text.
 */
function commerce_checkout_information_form_alter(&$form, &$form_state, $form_id) {

  if (strpos($form_id, 'commerce_checkout_form_') === 0) {

    // We don't know which panes we have on the current page
    // So we are going to check for them all here.
    $panes = commerce_checkout_panes(array('enabled' => TRUE));
    foreach ($panes as $pane_id => $pane) {
      global $language;
      $text = variable_get(COMMERCE_CHECKOUT_INFORMATION_VARIABLE_PREFIX . $pane_id, array());
      if (empty($text) || empty($text[$language->language]) || empty($text[$language->language]['value'])) {
        continue;
      }
      $text = $text[$language->language];
      if (isset($form[$pane_id])) {
        $form[$pane_id]['checkout_information_text_' . $pane_id] = array(
          '#type' => 'markup',
          '#markup' => theme('commerce_checkout_information_text', array(
            'text' => check_markup($text['value'], $text['format'], LANGUAGE_NONE),
          )),
          '#weight' => -200,
        );
      }
    }
  }
  elseif ($form_id == 'commerce_checkout_pane_settings_form') {

    $text = variable_get(COMMERCE_CHECKOUT_INFORMATION_VARIABLE_PREFIX . $form['checkout_pane']['#value']['pane_id'], array());

    if (!isset($form['settings'])) {
      $form['settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Checkout pane configuration'),
        '#description' => t('These settings are specific to this checkout pane.'),
        '#weight' => -1,
      );
      $form['display']['#weight'] = -2;
    }

    $form['settings']['checkout_information_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable information text for @pane', array('@pane' => $form['checkout_pane']['#value']['name'])),
      '#default_value' => (!empty($text)) ? 1 : 0,
    );

    $languages = language_list();
    $langoptions = array();
    foreach ($languages as $langcode => $language) {
      $langoptions[$langcode] = "{$language->name} ({$language->native})";
    }

    $form['settings']['checkout_information_language'] = array(
      '#type' => 'select',
      '#title' => t('Select language'),
      '#options' => $langoptions,
      '#states' => array(
        'visible' => array(
          ':input[name="checkout_information_enabled"]' => array('checked' => TRUE),
        ),
      ),
    );
    if (drupal_multilingual()) {
      $form['settings']['checkout_information_language']['#description'] = t('Select the language for which you want to enter the information, make sure to enter it in the native language');
    }
    else {
      $form['settings']['checkout_information_language']['#description'] = t('If you want to enter a information for another country, please enable it under <a href="@url">Languages</a>', array('@url' => url('admin/config/regional/language')));
    }

    foreach ($languages as $langcode => $language) {
      $form['settings']['checkout_information_container_' . $langcode] = array(
        '#type' => 'container',
        '#states' => array(
          'visible' => array(
            ':input[name="checkout_information_enabled"]' => array('checked' => TRUE),
            ':input[name="checkout_information_language"]' => array('value' => $langcode),
          ),
        ),
      );

      if (!$language->enabled) {
        $form['settings']['checkout_information_container_' . $langcode]['checkout_information_language_disabled'] = array(
          '#type' => 'markup',
          '#markup' => t('<p class="notice">This language is <strong>disabled</strong>, if you want to enable it go to <a href="@url">Languages</a>.</p>', array('@url' => url('admin/config/regional/language'))),
        );
      }

      // Want the "description" above the text field.
      $form['settings']['checkout_information_container_' . $langcode]['checkout_information_description'] = array(
        '#type' => 'markup',
        '#markup' => t('<p>Enter a piece of helpful information that help guide the user through the checkout flow.</p>'),
      );

      $form['settings']['checkout_information_container_' . $langcode]['checkout_information_text_' . $langcode] = array(
        '#title' => t('Information for @language', array('@language' => "{$language->name} ({$language->native})")),
        '#type' => 'text_format',
        '#default_value' => (!empty($text[$langcode])) ? $text[$langcode]['value'] : '',
        // Null is the default value.
        '#format' => (!empty($text[$langcode])) ? $text[$langcode]['format'] : NULL,
      );
    }

    // We add the submit handler to the beginning so we ensure it will
    // be executed before the pane's own submit handler have a
    // chance to redirect us.
    array_unshift($form['submit']['#submit'], 'commerce_checkout_information_checkout_pane_settings_form_submit');
  }
}

/**
 * Submit handler for checkout information boxes.
 */
function commerce_checkout_information_checkout_pane_settings_form_submit(&$form, &$form_state) {
  // If enabled we ensure that we set the variable else we delete it.
  if ($form_state['values']['checkout_information_enabled'] == 1) {
    $languages = language_list();
    $text = array();
    foreach ($languages as $langcode => $language) {
      $text[$langcode] = $form_state['values']['checkout_information_text_' . $langcode];
    }
    variable_set(COMMERCE_CHECKOUT_INFORMATION_VARIABLE_PREFIX . $form['checkout_pane']['#value']['pane_id'], $text);
  }
  else {
    variable_del(COMMERCE_CHECKOUT_INFORMATION_VARIABLE_PREFIX . $form['checkout_pane']['#value']['pane_id']);
  }
}

/**
 * Implements hook_theme().
 */
function commerce_checkout_information_theme($existing, $type, $theme, $path) {
  return array(
    'commerce_checkout_information_text' => array(
      'variables' => array('text' => NULL),
    ),
  );
}

/**
 * Implements hook_preprocess_HOOK().
 */
function commerce_checkout_information_preprocess_commerce_checkout_information_text(&$variables) {
  drupal_add_js(drupal_get_path('module', 'commerce_checkout_information') . '/js/commerce_checkout_information.js');
  $variables['attributes'] = array(
    'class' => array(
      // Used for attaching the javascript handler.
      'js-commerce-checkout-information',
    ),
  );
}

/**
 * Markup the information text popup.
 */
function theme_commerce_checkout_information_text($vars) {
  $text = $vars['text'];
  $attributes = drupal_attributes($vars['attributes']);

  if (empty($text)) {
    return '';
  }

  return "<div{$attributes}>{$text}</div>";
}
